package main

import (
	"fmt"
	"errors"
)

type Account struct{
    ID int
    amount float32
}

func (c *Account) sub(amount float32) (err error){
	if c.amount < amount{
		err = errors.New("Operational ERROR - Insufficient funds")
		return err
	}
	c.amount = c.amount - amount
	return err
}
func (c *Account) add(amount float32){
	c.amount = c.amount + amount
}
func (c *Account) print(){
	fmt.Printf("\nConta [%v] - SALDO: %v\n", c.ID,c.amount)

}