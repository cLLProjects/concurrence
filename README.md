# Concurrence
Foi implementado um programa para o projeto final da disciplina de Sistemas Operacinais que faz transferências de fundos entre duas contas ou mais contas em [GoLang](https://golang.org/), uma linguagem de programação criada pela Google e lançada em código livre em novembro de 2009. Golang, quando lançada, foi escrita na linguagem C. Mas depois, a linguagem passou a ser compilada pela propria linguagem Go, ou seja, Go é uma linguagem compilada.

# Instalação
A instalação da linguagem pode ser feita de várias maneiras podendo variar dependendo do Sistema Operacional, por isso esse é um [link](https://golang.org/doc/install) para o processo de instalação fornecido pelos próprios desenvolvedores da GoLang.

# Execução
Quando o código é compilado gera um executável, e ele será o programa em si.
* Para compilar é simples:  basta executar o comando **go build** na pasta onde os arquivos estão localizados 


*OBS: ( A pasta deve estar na GOPATH, qualquer dúvida a GoLang possui [documentação](https://golang.org/doc/)* )

# O Programa
O programa possui um menu auto explicativo. Que através dele podem ser demonstrados os requisitos impostos ao trabalho final da disciplina.
O menu possui as opções: 
* [1] Create Account
* [2] Make a Transaction
* [3] First Problem
* [4] Fifth Problem
* [5] Balance
* [6] EXIT

Além disso, foi implementada uma [Worker Pool](https://gobyexample.com/worker-pools) para otimizar o processamento, visto que um dos requisitos eram 100 transações simultaneas.

O problema da concorrência foi sanado pela função [Mutex](https://en.wikipedia.org/wiki/Mutual_exclusion), Mutual Exclusion, para não permitir acesso de diferentes threads a seções críticas (recursos compartilhados) ao mesmo tempo, o que causaria problemas (DeadLocks). Foi usada a biblioteca [Sync](https://golang.org/pkg/sync/#Mutex),  que possui a função de Mutex, além de outras primitivas de sincronização para se utilizar da melhor forma as threads virtuais da GoLang, as famosas [Go Routines](https://golang.org/ref/mem#tmp_5).

# Referências
* [Go Documentation](https://golang.org/doc/)
* [Go Tour](https://tour.golang.org)
* [A visual exemple of Go Routines concurrence](https://divan.dev/posts/go_concurrency_visualize/)
* [Go by Exemple](https://gobyexample.com)
