package main

import (
	"fmt"
	"log"
	"errors"
)
type Worker struct{
    ID int
	queue chan Transfer
	pool chan chan Transfer
}

func newWorker(ID int, pool chan chan Transfer) Worker{
	return Worker{
		ID : ID,
		queue: make(chan Transfer),
		pool : pool}
}

func (w Worker) start(){
	go func(){
        for{
			w.pool <- w.queue
			select {
			case transfer := <- w.queue:
				err := w.makeTransfer(transfer)
				if err != nil{
					fmt.Printf("[ERROR] - %v\n",err)
					log.Printf("Worker %v has a problem to transfer", w.ID)
				}
			}
        }
    }()
}

func (w Worker) makeTransfer(transfer Transfer) (err error){
	Mu.Lock()
	defer Mu.Unlock()
	//fmt.Printf("TRABALHADOR %v \n",w.ID)
	oID, dID, amount, time, _ := transfer.payload()
	if oID == dID{
		err = errors.New("Operational ERROR - Could not transfer to your own account")
		return err
	}
	err = Accounts[oID].sub(amount)
	if err != nil{
		return err
	}
	Accounts[dID].add(amount)
	log.Printf("Worker %v has transfer %v from account %v to account %v - %v\n", w.ID,amount,oID,dID,time)
	fmt.Printf("Transferido %v da conta %v para a conta %v - %v\n\n", amount,oID,dID,time)
	//Accounts[oID].print()
	//Accounts[dID].print()	
	return err
}