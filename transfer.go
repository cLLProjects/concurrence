package main

import (
	"time"
)
type Transfer struct{
    origin_Id int

    destiny_Id int

    amount float32

    date time.Time 
}

func (t Transfer) payload() (oridin_Id int, destiny_Id int, amount float32, date time.Time, err error){
	return t.origin_Id,t.destiny_Id,t.amount,t.date, nil
}
