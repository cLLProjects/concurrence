package main

import (
    "time"
    "fmt"
    "sync"
    "os/exec"
    "os"
    "log"
)

var (
    Accounts []*Account
    Mu sync.Mutex
    F *os.File
)
var Transfer_queue chan Transfer

var P *Pool

func clearScreen(sleep float32){
    if sleep > 0{
        time.Sleep(time.Duration(sleep) * time.Second)
    }
    cmd := exec.Command("clear") //Linux example, its tested
    cmd.Stdout = os.Stdout
    cmd.Run()
}

func main (){
    F, err := os.OpenFile("log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("[ERROR] - Opening Log File Fail: %v", err)
    }
    defer F.Close()
    log.SetOutput(F)
    Accounts = append(Accounts, &Account{ID:0, amount:100})
    Accounts = append(Accounts, &Account{ID:1,amount:100})
    Accounts = append(Accounts, &Account{ID:2,amount:100})
    Transfer_queue = make(chan Transfer)
    var option string
    var transC1 int
    var transC2 int
    var transAmount float32
    P = New(100)
    P.run()
    clearScreen(0)
    for{
        fmt.Printf("[1] Create Account\n[2] Make a Transaction\n[3] First Problem\n"+
        "[4] Fifth Problem\n\n[5] Balance\n"+
        "[6] EXIT\n")
        fmt.Printf("Accounts: %v \n", Accounts)
        fmt.Scan(&option)
        switch option{
        case "1":
            Accounts = append(Accounts, &Account{ID:len(Accounts)+1, amount:100})
            fmt.Printf("Account created successfully\n")
        case "2":
            fmt.Printf("From account(ID):\n")
            fmt.Scan(&transC1)
            fmt.Printf("To account(ID):\n")
            fmt.Scan(&transC2)
            fmt.Printf("Ammount:\n")
            fmt.Scan(&transAmount)
            putOnQueue(transC1,transC2,transAmount)
        case "3":
            fmt.Printf("Transferring 10 from account 0 to account 1 and account 2:\n")
            go putOnQueue(0,2,10)
            go putOnQueue(0,1,10)
        case "4":
            for i := 0; i < 50; i++{
                go putOnQueue(2,0,1)
                go putOnQueue(0,1,1)
            }
        case "5":
            clearScreen(1)
            for i, count := range Accounts{
                fmt.Printf("[Account ID] - %v \n[Account Balance] - %v \n\n",i,count.amount)
            }
            fmt.Printf("Press enter to turn back\n")
            fmt.Scanln()
        case "6":
            fmt.Printf("EXITING...\n")
            return
        default:
            fmt.Printf("Please select a valid option")
        }
        clearScreen(2)        
        }
    
        
}
func putOnQueue(c1 int, c2 int, amount float32){
    Mu.Lock()
	defer Mu.Unlock()
    Transfer_queue <- Transfer{origin_Id:c1,destiny_Id:c2,amount: amount,date: time.Now()}
}