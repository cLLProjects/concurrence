package main

import (
    "fmt"
)

type Count struct{
    ID int
    amount float32
}

func (c *Count) sub(amount float32){
	c.amount = c.amount - amount
}
func (c *Count) add(amount float32){
	c.amount = c.amount + amount
}
func (c *Count) print(){
	fmt.Printf("\nConta [%v] - SALDO: %v\n", c.ID,c.amount)

}