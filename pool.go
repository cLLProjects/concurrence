package main

type Pool struct{
    workerCount int
    workerPool chan chan Transfer
}

func New(workerCount int) *Pool{
	workerpool := make(chan chan Transfer, workerCount)

	return &Pool{workerPool: workerpool, workerCount: workerCount}

}

func (p *Pool) run(){
	for i := 0; i< p.workerCount; i++{
		worker := newWorker(i, p.workerPool)
		worker.start()
	}
	go p.catchTransfer()


}

func (p *Pool) catchTransfer(){
	for{
		select{
		case transfer := <-Transfer_queue:
			go func(transfer Transfer){
				worker := <-p.workerPool
				worker <- transfer
			}(transfer)
		}
	}
	
}